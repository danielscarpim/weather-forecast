# Weather Forecast App

This app displays data from OpenWeatherMap api.
In order to get around CORS issues, it loads the data through a very simple nodejs middleware.

## Running the app

### `yarn start`

Runs both the app at [http://localhost:3000](http://localhost:3000), and the backend middleware at `http://localhost:4000`.

## Run unit tests

### `yarn test:ci`

Runs tests and generates test coverage report. The app is currently at 100% coverage. It requires at least 80% in order to pass the test.
