import axios from 'axios';
import { ForecastApiResponse } from 'types/Forecast';

const apiKey = process.env.REACT_APP_API_KEY;

const openWeatherMapAPI = axios.create({
  baseURL: 'http://localhost:4000/api',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const getForecast = async (): Promise<ForecastApiResponse> => {
  const query = 'M%C3%BCnchen,DE';
  const response = await openWeatherMapAPI.get(`/forecast`, {
    params: { q: query, appid: apiKey },
  });
  return response.data;
};
