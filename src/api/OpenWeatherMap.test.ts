import { mockForecast } from '__mocks__/mockForecast';
import { getForecast } from './OpenWeatherMap';

describe('getForecast', () => {
  it('Fetches forecast data', async () => {
    const result = await getForecast();
    expect(result).toStrictEqual(mockForecast);
  });
});
