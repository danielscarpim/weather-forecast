import React from 'react';
import { colors } from 'theme';
import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  backdrop: {
    backgroundColor: colors.indigo[100],
    zIndex: 100,
    position: 'absolute',
  },
});

interface LoadingProps {
  open?: boolean;
}

export const Loading: React.FC<LoadingProps> = ({ open = true }) => {
  const classes = useStyles();
  return (
    <Backdrop data-testid="Loading" className={classes.backdrop} open={open}>
      <CircularProgress color="primary" />
    </Backdrop>
  );
};
