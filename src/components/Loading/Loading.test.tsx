import React from 'react';
import { render } from '@testing-library/react';
import { Loading } from './Loading';

describe('Loading', () => {
  it('Renders succesfully', () => {
    const { container, getByTestId } = render(<Loading />);
    expect(container).toBeVisible();
    expect(getByTestId('Loading')).toBeVisible();
  });

  it('Stays hidden when open is false', () => {
    const { container, getByTestId } = render(<Loading open={false} />);
    expect(container).toBeVisible();
    expect(getByTestId('Loading')).not.toBeVisible();
  });
});
