import { mocked } from 'ts-jest/utils';
import { renderHook } from '@testing-library/react-hooks';
import { mockForecast, mockCity, mockForecastList } from '__mocks__/mockForecast';
import { getForecast } from 'api/OpenWeatherMap';
import { useApp } from './useApp';

jest.mock('api/OpenWeatherMap');
const mockedGetForecast = mocked(getForecast, true);

describe('useApp', () => {
  it('Fetches and returns data from API', async () => {
    mockedGetForecast.mockResolvedValue(mockForecast);
    const { result, waitForNextUpdate } = renderHook(() => useApp());
    await waitForNextUpdate();
    expect(result.current.city).toBe(mockCity);
    expect(result.current.forecastList).toBe(mockForecastList);
  });

  it('Returns error message', async () => {
    mockedGetForecast.mockRejectedValue('error');
    const { result, waitForNextUpdate } = renderHook(() => useApp());
    await waitForNextUpdate();
    expect(result.current.city).toBe(undefined);
    expect(result.current.forecastList).toBe(undefined);
    expect(result.current.error).toBe('Something went wrong. Please try again later.');
  });
});
