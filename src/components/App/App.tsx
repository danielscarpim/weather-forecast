import React from 'react';
import { Box, Container, Typography } from '@material-ui/core';
import { colors, spacing } from 'theme';
import { ForecastList } from 'components/ForecastList/ForecastList';
import { Loading } from 'components/Loading/Loading';
import { SelectedForecast } from 'components/SelectedForecast/SelectedForecast';
import { useApp } from './useApp';

export const App: React.FC = () => {
  const { forecastList, city, loading, setSelected, selected, error } = useApp();
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100%"
    >
      <Container maxWidth="md">
        <Box
          data-testid="App"
          bgcolor={colors.indigo[100]}
          width="100%"
          height={`${spacing(64)}px`}
          borderRadius={`${spacing()}px`}
          position="relative"
          overflow="hidden"
          display="flex"
          flexDirection="column"
          justifyContent="center"
        >
          {forecastList && city && selected && !error && (
            <Box>
              <SelectedForecast
                data-testid="SelectedForecast"
                forecast={selected}
                city={city}
              />
              <ForecastList
                data-testid="ForecastList"
                forecastList={forecastList}
                selected={selected}
                setSelected={setSelected}
              />
            </Box>
          )}
          {error && (
            <Box textAlign="center">
              <Typography data-testid="ErrorMessage" variant="caption">
                {error}
              </Typography>
            </Box>
          )}
          <Loading open={loading} />
        </Box>
      </Container>
    </Box>
  );
};
