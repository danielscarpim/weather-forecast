import React from 'react';
import { mocked } from 'ts-jest/utils';
import { mockCity, mockForecastList } from '__mocks__/mockForecast';
import { render } from '@testing-library/react';
import { useApp } from './useApp';
import { App } from './App';

jest.mock('./useApp');
const mockedUseApp = mocked(useApp, true);
const mockedUseAppReturn = {
  forecastList: mockForecastList,
  city: mockCity,
  loading: false,
  selected: mockForecastList[0],
  error: '',
  setSelected: jest.fn,
};

describe('App', () => {
  it('Renders succesfully', () => {
    mockedUseApp.mockReturnValue(mockedUseAppReturn);
    const { container, getByTestId } = render(<App />);
    expect(container).toBeVisible();
    expect(getByTestId('ForecastList')).toBeVisible();
    expect(getByTestId('SelectedForecast')).toBeVisible();
  });
  it('Displays error message', () => {
    mockedUseApp.mockReturnValue({ ...mockedUseAppReturn, error: 'Oh no!' });
    const { getByTestId } = render(<App />);
    expect(getByTestId('ErrorMessage')).toBeVisible();
  });
});
