import { useEffect, useState } from 'react';
import { getForecast } from 'api/OpenWeatherMap';
import { City, Forecast } from 'types';

export const useApp = () => {
  const [forecastList, setForecastList] = useState<Forecast[]>();
  const [city, setCity] = useState<City>();
  const [loading, setLoading] = useState(false);
  /*
    TODO:
    Refactor this to use a global state with useContext,
    instead of passing this values down to the ForecastListItem component
  */
  const [selected, setSelected] = useState<Forecast>();

  const [error, setError] = useState('');

  const fetchForecastList = async () => {
    setLoading(true);
    try {
      const response = await getForecast();
      setForecastList(response.list);
      setSelected(response.list[0]);
      setCity(response.city);
    } catch {
      setError('Something went wrong. Please try again later.');
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchForecastList();
  }, []);

  return {
    forecastList,
    city,
    loading,
    selected,
    error,
    setSelected,
  };
};
