import React from 'react';
import { Box, Typography, Button, withStyles } from '@material-ui/core';
import { SunIcon } from 'components/SunIcon/SunIcon';
import { spacing, colors } from 'theme';
import { celsius, date } from 'utils';
import { Forecast } from 'types';

interface IForecastListItem {
  forecast: Forecast;
  selected: Forecast;
  setSelected: (forecast: Forecast) => void;
}

const ForecastButton = withStyles(() => ({
  root: {
    backgroundColor: 'transparent',
    boxSizing: 'content-box',
    boxShadow: 'none',
    padding: `${spacing(2)}px ${spacing(2.75)}px`,
    '&:hover': {
      backgroundColor: colors.indigo[80],
    },
    '&.selected': {
      backgroundColor: colors.indigo[70],
    },
  },
}))(Button);

export const ForecastListItem: React.FC<IForecastListItem> = ({
  forecast,
  selected,
  setSelected,
}) => {
  const { main, weather, dt_txt: dtTxt } = forecast;
  const { temp } = main;
  const { main: sky } = weather[0];

  return (
    <ForecastButton
      variant="contained"
      onClick={() => setSelected(forecast)}
      className={forecast.dt === selected.dt ? 'selected' : ''}
      data-testid="ForecastButton"
    >
      <Box
        data-testid="ForecastListItem"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Typography variant="caption">{date(dtTxt, 'hh:00')}</Typography>
        <Box height={`${spacing(8)}px`} width={`${spacing(8)}px`} my={`${spacing(1)}px`}>
          <SunIcon sky={sky} />
        </Box>
        <Box ml={`${spacing(2)}px`}>
          <Typography>{celsius(temp)}</Typography>
        </Box>
      </Box>
    </ForecastButton>
  );
};
