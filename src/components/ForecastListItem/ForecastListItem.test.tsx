import React from 'react';
import { render } from '@testing-library/react';
import { mockForecastList } from '__mocks__/mockForecast';
import { ForecastListItem } from './ForecastListItem';

const mockSetSelected = jest.fn();

describe('ForecastListItem', () => {
  it('Renders succesfully', () => {
    const { container } = render(
      <ForecastListItem
        forecast={mockForecastList[0]}
        selected={mockForecastList[0]}
        setSelected={mockSetSelected}
      />,
    );
    expect(container).toBeVisible();
  });

  it('Sets selected on click', () => {
    const { getByTestId } = render(
      <ForecastListItem
        forecast={mockForecastList[0]}
        selected={mockForecastList[0]}
        setSelected={mockSetSelected}
      />,
    );
    getByTestId('ForecastButton').click();
    expect(mockSetSelected).toBeCalledWith(mockForecastList[0]);
  });
});
