import React from 'react';
import { render } from '@testing-library/react';
import { mockForecastList, mockCity } from '__mocks__/mockForecast';
import { SelectedForecast } from './SelectedForecast';

describe('SelectedForecast', () => {
  it('Renders succesfully', () => {
    const { container } = render(
      <SelectedForecast forecast={mockForecastList[0]} city={mockCity} />,
    );
    expect(container).toBeVisible();
  });
});
