import React from 'react';
import { Forecast, City } from 'types';
import { spacing } from 'theme';
import { Box, Typography } from '@material-ui/core';
import { SunIcon } from 'components/SunIcon/SunIcon';
import { celsius, date } from 'utils';

interface ISelectedForecast {
  forecast: Forecast;
  city: City;
}

export const SelectedForecast: React.FC<ISelectedForecast> = ({ forecast, city }) => {
  const { dt_txt: dtTxt, main, weather } = forecast;
  const { temp, temp_min: tempMin, temp_max: tempMax } = main;
  const { main: sky } = weather[0];
  return (
    <Box
      data-testid="SelectedForecast"
      display="flex"
      padding={`${spacing(4)}px ${spacing(8)}px`}
      justifyContent="space-between"
    >
      <Box width={`${spacing(22)}px`} height={`${spacing(22)}px`}>
        <SunIcon sky={sky} />
      </Box>
      <Box
        width={`${spacing(22)}px`}
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Box display="flex" justifyContent="space-between" width="100%">
          <Typography variant="caption">{sky}</Typography>
          <Typography variant="caption">{`${celsius(tempMax)}/${celsius(
            tempMin,
          )}`}</Typography>
        </Box>
        <Typography variant="h1">{celsius(temp)}</Typography>
      </Box>
      <Box>
        <Typography variant="caption">{city.name}</Typography>
        <Typography variant="h2">{date(dtTxt, 'EEEE')}</Typography>
        <Typography variant="h2">{date(dtTxt, 'd. MMMM')}</Typography>
      </Box>
    </Box>
  );
};
