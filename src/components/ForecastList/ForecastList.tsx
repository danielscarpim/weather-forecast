import React from 'react';
import { Box } from '@material-ui/core';
import { Forecast } from 'types';
import { ForecastListItem } from 'components/ForecastListItem/ForecastListItem';

interface IForecastList {
  forecastList: Forecast[];
  selected: Forecast;
  setSelected: (forecast: Forecast) => void;
}

export const ForecastList: React.FC<IForecastList> = ({
  forecastList,
  selected,
  setSelected,
}) => {
  return (
    <Box overflow="hidden">
      <Box data-testid="ForecastList" display="flex">
        {forecastList.map((forecast) => (
          <ForecastListItem
            data-testid="ForecastListItem"
            key={forecast.dt}
            forecast={forecast}
            selected={selected}
            setSelected={setSelected}
          />
        ))}
      </Box>
    </Box>
  );
};
