import React from 'react';
import { render } from '@testing-library/react';
import { mockForecastList } from '__mocks__/mockForecast';
import { ForecastList } from './ForecastList';

describe('ForecastList', () => {
  it('Renders succesfully', () => {
    const { container, getAllByTestId } = render(
      <ForecastList
        forecastList={mockForecastList}
        selected={mockForecastList[0]}
        setSelected={jest.fn()}
      />,
    );
    expect(container).toBeVisible();
    expect(getAllByTestId('ForecastListItem').length).toBe(5);
  });
});
