import React from 'react';
import { Cloud, Sun } from 'assets';

interface ISunIcon {
  sky: string;
}

export const SunIcon: React.FC<ISunIcon> = ({ sky }) => {
  return (
    <img
      data-testid="IconImage"
      width="100%"
      height="100%"
      src={sky === 'Clear' ? Sun : Cloud}
      alt={sky}
    />
  );
};
