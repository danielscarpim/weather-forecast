import React from 'react';
import { render } from '@testing-library/react';
import { SunIcon } from './SunIcon';

describe('SunIcon', () => {
  it('Displays Sun icon', () => {
    const { getByTestId } = render(<SunIcon sky="Clear" />);
    expect(getByTestId('IconImage')).toHaveAttribute('src', 'weather-sun.svg');
  });

  it('Displays Cloud icon', () => {
    const { getByTestId } = render(<SunIcon sky="Rain" />);
    expect(getByTestId('IconImage')).toHaveAttribute('src', 'weather-cloud.svg');
  });
});
