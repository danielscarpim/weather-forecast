import { mockForecast } from '__mocks__/mockForecast';

export default {
  interceptors: {
    request: { use: jest.fn(), eject: jest.fn() },
    response: { use: jest.fn(), eject: jest.fn() },
  },
  create: () => ({
    interceptors: {
      request: {
        use: jest.fn(),
      },
      response: {
        use: jest.fn(),
      },
    },
    getUri: jest.fn(),
    request: jest.fn(() => Promise.resolve()),
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    get: (route: string) => {
      if (route.includes(`/forecast`)) {
        return Promise.resolve({ data: mockForecast });
      }
      return Promise.resolve({ data: '' });
    },
  }),
};
