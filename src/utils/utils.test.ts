import { celsius, date } from './utils';

describe('celsius', () => {
  it('Converts Kelvin to Celsius', () => {
    const temp = celsius(285.66);
    expect(temp).toBe('13°');
  });
});

describe('date', () => {
  it('formats date with date-fns', () => {
    const formattedDate = date('2017-02-16 12:00:00', 'EEEE, dd. MMMM yyyy');
    expect(formattedDate).toBe('Thursday, 16. February 2017');
  });
});
