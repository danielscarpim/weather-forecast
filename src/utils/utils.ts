import { format as dateFnsFormat } from 'date-fns';

export const celsius = (value: number) => {
  const metric = Math.round(value - 273.15);
  return `${metric}°`;
};

export const date = (value: string, format: string) => {
  return dateFnsFormat(new Date(value), format);
};
