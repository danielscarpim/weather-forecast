import { createTheme } from '@material-ui/core/styles';

// https://material-ui.com/components/about-the-lab/#typescript
// eslint-disable-next-line import/no-unresolved
import type {} from '@material-ui/lab/themeAugmentation';

export const colors = {
  indigo: {
    30: '#A8AABD',
    70: '#51557A',
    80: '#3B3F69',
    100: '#262A59',
  },
  white: '#ffffff',
  black: '#000000',
  gray: '#E5E5E5',
};

export const space = 8;

export const spacing = (multiplier = 1): number => multiplier * space;

export const theme = createTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        html: {
          height: '100%',
        },
        body: {
          backgroundColor: colors.gray,
          height: '100%',
        },
        '#root': {
          height: '100%',
        },
      },
    },
    MuiTypography: {
      root: {
        color: colors.white,
      },
      h1: {
        fontSize: `${spacing(16)}px`,
        fontWeight: 'bold',
      },
      h2: {
        fontSize: `${spacing(7)}px`,
        fontWeight: 'bold',
      },
      caption: {
        color: colors.indigo[30],
        fontSize: `${spacing(3)}px`,
      },
      body1: {
        fontSize: `${spacing(4)}px`,
        fontWeight: 'bold',
      },
    },
  },
  spacing,
});
