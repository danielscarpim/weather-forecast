/* eslint-disable @typescript-eslint/no-var-requires */
import fetch from 'node-fetch';
import express from 'express';
import cors from 'cors';

const app = express();

app.use(cors());
app.get('/api/forecast', async (req, res) => {
  const { q, appid } = req.query;
  const api = `https://samples.openweathermap.org/data/2.5/forecast?q=${q}&appid=${appid}`;
  const response = await fetch(api);
  const body = await response.json();
  res.json(body);
});
app.listen(4000, () => {
  // eslint-disable-next-line no-console
  console.log('Server listening on port 4000');
});
